import { Box, Typography } from '@mui/material';

interface Props {
  icon: string;
  text: string;
}

const IconWithText: React.FC<Props> = ({ icon, text }) => (
  <Box my={2} display="flex" alignItems="center" justifyContent="center">
    <i style={{ fontSize: 30 }} className={`wi wi-${icon}`}></i>
    <Typography ml={3}>{text}</Typography>
  </Box>
);

export default IconWithText;
