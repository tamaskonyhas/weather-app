import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import { Box, IconButton } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { COLORS } from '../utils/constants';

const BackButton: React.FC = () => {
  const navigate = useNavigate();

  return (
    <Box textAlign="left">
      <IconButton onClick={() => navigate('/')}>
        <ChevronLeftIcon sx={{ color: COLORS.lightBlue, fontSize: 48 }} />
      </IconButton>
    </Box>
  );
};

export default BackButton;
