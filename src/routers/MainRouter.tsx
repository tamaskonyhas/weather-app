import { Route, Routes } from 'react-router-dom';
import Cities from '../pages/Cities';
import City from '../pages/City';
import Search from '../pages/Search';

const MainRouter: React.FC = () => (
  <Routes>
    <Route path="*" element={<Cities />} />
    <Route path="/search" element={<Search />} />
    <Route path="/city/:city" element={<City />} />
  </Routes>
);

export default MainRouter;
