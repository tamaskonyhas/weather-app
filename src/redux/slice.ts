import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

interface State {
  cities: string[];
  selectedCities: string[];
  isLoading: boolean;
}

const initialState: State = {
  selectedCities: ['Budapest'],
  cities: [],
  isLoading: false,
};

const slice = createSlice({
  name: 'cities',
  initialState,
  reducers: {
    selectCity: (state, { payload }: PayloadAction<string>) => {
      state.selectedCities = [...state.selectedCities, payload];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCities.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchCities.fulfilled, (state, action) => {
        state.isLoading = false;
        state.cities = state.cities.concat(action.payload);
      });
  },
});

export const fetchCities = createAsyncThunk('cities/fetchCities', async () => {
  const response = await fetch('data.json');
  const json = (await response.json()) as { country: string; city: string }[];
  return json.map(({ city }) => city);
});

export const { actions, reducer } = slice;
