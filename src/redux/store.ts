import { configureStore } from '@reduxjs/toolkit';
import { reducer } from './slice';

const state = localStorage.getItem('reduxState');

const store = configureStore({
  preloadedState: state ? JSON.parse(state) : {},
  reducer: {
    cities: reducer,
  },
});

store.subscribe(() => {
  localStorage.setItem('reduxState', JSON.stringify(store.getState()));
});

export default store;
