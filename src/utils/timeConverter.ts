const timeConverter = (timestamp: number) => {
  const hours = new Date(timestamp * 1000).getUTCHours();
  const minutes = new Date(timestamp * 1000).getUTCMinutes();

  return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
};

export default timeConverter;
