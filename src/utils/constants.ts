export const WEATHER_API_URL = 'https://api.openweathermap.org/data/2.5';

export const COLORS = {
  lightBlue: '#92b7cc',
  green: '#03fe41',
};

export const MOCK_DATA = {
  coord: {
    lon: 19.0399,
    lat: 47.498,
  },
  weather: [
    {
      id: 800,
      main: 'Clear',
      description: 'clear sky',
      icon: '01n',
    },
  ],
  base: 'stations',
  main: {
    temp: 2.64,
    feels_like: -1.94,
    temp_min: 1.71,
    temp_max: 3.58,
    pressure: 1013,
    humidity: 71,
  },
  visibility: 10000,
  wind: {
    speed: 5.66,
    deg: 300,
  },
  clouds: {
    all: 0,
  },
  dt: 1677973720,
  sys: {
    type: 2,
    id: 2009313,
    country: 'HU',
    sunrise: 1677993469,
    sunset: 1678034003,
  },
  timezone: 3600,
  id: 3054643,
  name: 'Budapest',
  cod: 200,
};

export type WeatherDataType = typeof MOCK_DATA;
