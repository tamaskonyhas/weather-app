import { Box, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { WeatherDataType } from '../../../utils/constants';

const Clock: React.FC<{ weatherData: WeatherDataType }> = ({ weatherData }) => {
  const [time, setTime] = useState({
    hours: 0,
    minutes: 0,
  });

  useEffect(() => {
    const timestamp = weatherData.dt + weatherData.timezone;
    const date = new Date(timestamp * 1000);
    setTime({
      hours: date.getUTCHours(),
      minutes: date.getUTCMinutes(),
    });

    const intervalID = setInterval(() => {
      setTime((prevTime) => {
        let newMinutes = prevTime.minutes + 1;
        let newHours = prevTime.hours;

        if (newMinutes === 60) {
          newMinutes = 0;
          newHours = prevTime.hours + 1;
        }

        if (newHours === 24) {
          newHours = 0;
        }

        return {
          hours: newHours,
          minutes: newMinutes,
        };
      });
    }, 60000);

    return () => {
      clearInterval(intervalID);
    };
  }, [weatherData]);

  return (
    <Box>
      <Typography sx={{ fontSize: 64, lineHeight: '64px' }}>
        {time.hours < 10 ? '0' : ''}
        {time.hours}
      </Typography>
      <Typography sx={{ fontSize: 64, lineHeight: '64px' }}>
        {time.minutes < 10 ? '0' : ''}
        {time.minutes}
      </Typography>
    </Box>
  );
};

export default Clock;
