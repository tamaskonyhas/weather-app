import { Box, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import BackButton from '../../components/BackButton';
import IconWithText from '../../components/IconWithText';
import { WeatherDataType, WEATHER_API_URL } from '../../utils/constants';
import timeConverter from '../../utils/timeConverter';
import Clock from './components/Clock';
import { renderIcon } from './utils/renderIcon';

const City: React.FC = () => {
  const { city } = useParams();
  const [data, setData] = useState<WeatherDataType | undefined>();
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState<{ cod: string; message: string }>();

  useEffect(() => {
    const getData = async () => {
      try {
        setLoading(true);
        const data = await fetch(
          `${WEATHER_API_URL}/weather?q=${city}&appid=${process.env.REACT_APP_WEATHER_API_KEY}&units=metric`
        );
        const json = await data.json();
        if (json.cod !== 200 || json.message) {
          setError(json);
        } else {
          setData(json);
        }
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    };

    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) return <Typography>Loading...</Typography>;
  if (!data || error)
    return (
      <Box>
        <BackButton />
        <Typography>Something went wrong!</Typography>
        {error && (
          <Typography>
            Error code: {error?.cod}, Error message: {error?.message}
          </Typography>
        )}
      </Box>
    );

  return (
    <Box>
      <BackButton />
      <Clock weatherData={data} />
      <Typography sx={{ fontSize: 32, fontWeight: 'bold', mb: 6 }}>{city}</Typography>
      {renderIcon(data)}
      <Typography mt={1} mb={4}>
        {data.weather[0].description}
      </Typography>
      <IconWithText icon="thermometer" text={`${data.main.temp} °C`} />
      <IconWithText icon="sunrise" text={timeConverter(data.sys.sunrise + data.timezone)} />
      <IconWithText icon="sunset" text={timeConverter(data.sys.sunset + data.timezone)} />
    </Box>
  );
};

export default City;
