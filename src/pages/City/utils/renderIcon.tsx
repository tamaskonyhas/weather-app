import { WeatherDataType } from '../../../utils/constants';

export const renderIcon = (data: WeatherDataType) => {
  let iconCode;
  if (!data) return;
  const weatherId = data.weather[0].id;
  switch (true) {
    case weatherId >= 200 && weatherId < 300:
      iconCode = 'thunderstorm';
      break;
    case weatherId >= 300 && weatherId < 400:
      iconCode = 'showers';
      break;
    case weatherId >= 500 && weatherId < 600:
      iconCode = 'rain';
      break;
    case weatherId >= 600 && weatherId < 700:
      iconCode = 'snow';
      break;
    case weatherId >= 700 && weatherId <= 800:
      iconCode = 'sunny';
      break;
    case weatherId > 800:
      iconCode = 'cloudy';
      break;
    default:
      iconCode = 'sunny';
  }

  return <i style={{ fontSize: 56 }} className={`wi wi-day-${iconCode}`}></i>;
};
