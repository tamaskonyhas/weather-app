import { Box, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useAppSelector } from '../redux/hooks';
import { COLORS } from '../utils/constants';

const styles = {
  button: {
    fontSize: 26,
    width: 'fit-content',
    margin: 'auto',
    px: 2,
    minWidth: 200,
  },
};

const Cities: React.FC = () => {
  const navigate = useNavigate();
  const selectedCities = useAppSelector((state) => state.cities.selectedCities);

  return (
    <Box display="flex" flexDirection="column">
      {selectedCities.map((city) => (
        <Button
          sx={{ ...styles.button, color: COLORS.lightBlue }}
          key={city}
          onClick={() => navigate(`/city/${city}`)}
        >
          {city}
        </Button>
      ))}
      <Button sx={{ ...styles.button, color: COLORS.green }} onClick={() => navigate('/search')}>
        +
      </Button>
    </Box>
  );
};

export default Cities;
