import { Box, Button } from '@mui/material';
import React, { Fragment } from 'react';
import { COLORS } from '../../../utils/constants';

const styles = {
  listItem: {
    fontSize: 26,
    width: 'fit-content',
    margin: 'auto',
    px: 2,
    minWidth: 200,
  },
};

interface Props {
  city: string;
  search: string;
  onCitySelect: (city: string) => void;
}

const HighlightedButton: React.FC<Props> = ({ city, search, onCitySelect }) => {
  const regex = new RegExp(search, 'gi');
  const parts = city.split(regex);

  return (
    <Button sx={styles.listItem} onClick={() => onCitySelect(city)}>
      {parts.map((part, index) => (
        <Fragment key={index}>
          {part}
          {index < parts.length - 1 && <Box sx={{ color: COLORS.lightBlue }}>{search}</Box>}
        </Fragment>
      ))}
    </Button>
  );
};

export default HighlightedButton;
