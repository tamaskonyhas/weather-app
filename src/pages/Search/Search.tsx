import { Box, Button, Input } from '@mui/material';
import BackButton from '../../components/BackButton';
import { COLORS } from '../../utils/constants';
import HighlightedButton from './components/HighlightedButton';
import useSearch from './useSearch';

const styles = {
  input: {
    border: 'none',
    boxShadow: 'none',
    width: 300,
    margin: 'auto',
    color: COLORS.lightBlue,
    fontSize: 24,
  },
};

const Search: React.FC = () => {
  const {
    searchValue,
    selectedCapital,
    onSave,
    filteredCities,
    onInputChange,
    onCitySelect,
    isLoading,
  } = useSearch();

  return (
    <Box display="flex" flexDirection="column">
      <BackButton />
      <Input
        placeholder="Search..."
        sx={styles.input}
        disabled={isLoading}
        value={searchValue}
        onChange={onInputChange}
      />
      {filteredCities.map((city) => (
        <HighlightedButton
          key={city}
          search={searchValue}
          city={city}
          onCitySelect={onCitySelect}
        />
      ))}
      {selectedCapital && (
        <Box sx={{ textAlign: 'right' }}>
          <Button onClick={onSave}>SAVE</Button>
        </Box>
      )}
    </Box>
  );
};

export default Search;
