import { ChangeEvent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { actions, fetchCities } from '../../redux/slice';

const useSearch = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { cities, selectedCities, isLoading } = useAppSelector((state) => state.cities);

  const [searchValue, setSearchValue] = useState('');

  const [filteredCities, setFilteredCities] = useState<string[]>([]);
  const [selectedCapital, setSelectedCapital] = useState('');

  useEffect(() => {
    if (!searchValue.length) return;
    const filtered = cities
      .filter(
        (city) =>
          city.toUpperCase().includes(searchValue.toUpperCase()) && !selectedCities.includes(city)
      )
      .slice(0, 8);
    setFilteredCities(filtered);
  }, [cities, selectedCities, searchValue]);

  useEffect(() => {
    if (!cities.length) {
      dispatch(fetchCities());
    }
  }, [cities.length, dispatch]);

  const onSave = () => {
    dispatch(actions.selectCity(selectedCapital));
    navigate(`/`);
  };

  const onInputChange = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(value);
    setSelectedCapital('');

    if (!value) {
      setFilteredCities([]);
    }
  };

  const onCitySelect = (city: string) => {
    setSelectedCapital(city);
    setSearchValue(city);
  };

  return {
    searchValue,
    selectedCapital,
    onSave,
    filteredCities,
    onInputChange,
    onCitySelect,
    isLoading,
  };
};

export default useSearch;
