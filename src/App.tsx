import { Box } from '@mui/material';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store from './redux/store';
import MainRouter from './routers/MainRouter';

const App: React.FC = () => (
  <BrowserRouter>
    <Provider store={store}>
      <Box my={4} mx={[3, 'auto']} maxWidth={600} textAlign="center">
        <MainRouter />
      </Box>
    </Provider>
  </BrowserRouter>
);

export default App;
